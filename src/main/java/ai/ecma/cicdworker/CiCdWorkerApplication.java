package ai.ecma.cicdworker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiCdWorkerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CiCdWorkerApplication.class, args);
    }

}
