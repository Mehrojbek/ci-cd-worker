package ai.ecma.cicdworker.controller;

import ai.ecma.cicdworker.payload.ApiResult;
import ai.ecma.cicdworker.service.ApplicationRunService;
import ai.ecma.cicdworker.service.TestLocalService;
import ai.ecma.cicdworker.utils.AppConstant;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping(AppConstant.BASE_PATH)
public class GitlabMergeController {
    private final ApplicationRunService applicationRunService;
    private final TestLocalService testLocalService;

    @PostMapping("/get-gitlab-update")
    ApiResult<?> getGitlabUpdate(@RequestBody Map<String,Object> map){
        System.out.println(map);
        System.out.println("====================");
        return applicationRunService.stopAndRunApplication(map);
    }


    @GetMapping("/test")
    public void testRun(){
        //ACADEMIC_CONTENT_SERVICE DAI CICDExecutor file NI EXECUTE QILADI
        StringBuilder runningLog = new StringBuilder();
        applicationRunService.runService(
                AppConstant.ACADEMIC_CONTENT_SERVICE,
                AppConstant.MASTER,
                AppConstant.ACADEMIC_CONTENT_SERVICE_PORT_FIRST,
                AppConstant.ACADEMIC_CONTENT_SERVICE_PORT_SECOND,
                AppConstant.ACADEMIC_CONTENT_SERVICE_JAR_NAME,
                runningLog,
                AppConstant.ACADEMIC_SERVICE_NAME_ON_SERVICE_REGISTRY,
                ApplicationRunService.activeProfile
        );
    }

    @GetMapping("/set-wait-time")
    public long setWaitTime(@RequestParam long milliSecond){
        ApplicationRunService.milliSecund = milliSecond;
        return ApplicationRunService.milliSecund;
    }

    @GetMapping("/local-test")
    public void localTest(){
        testLocalService.testRun();
    }

    @GetMapping("/change-profile/{profile}")
    public String changeProfile(@PathVariable String profile){
        ApplicationRunService.activeProfile = profile;
        return ApplicationRunService.activeProfile;
    }

}
