package ai.ecma.cicdworker.utils;

public interface AppConstant {
    String BASE_PATH = "/api/ci-cd-worker";

    String BASE_PATH_UBUNTU_ERP = "/home/user/erp/";

    String RUNNER_CLASS_NAME = "CICDExecutor";

    String JAVA_EXTENSION = ".java";

    String ACADEMIC_CONTENT_SERVICE = "app-academic-content";

    String ACADEMIC_SERVICE_NAME_ON_SERVICE_REGISTRY = "ACADEMIC-CONTENT-SERVICE";
    String SALES_SERVICE_NAME_ON_SERVICE_REGISTRY = "SALES-SERVICE";

    String SALES_SERVICE = "app-sales";
    String HELLO_TEST_PROJECT = "test-ci-cd";
    String MAIN = "main";
    String MASTER = "master";

    String ACADEMIC_CONTENT_SERVICE_PORT_FIRST = "8090";
    String ACADEMIC_CONTENT_SERVICE_PORT_SECOND = "8091";

    String SALES_SERVICE_FIRST_PORT = "8180";
    String SALES_SERVICE_SECOND_PORT = "8181";

    String ACADEMIC_CONTENT_SERVICE_JAR_NAME = "academic-content";
    String SALES_SERVICE_JAR_NAME = "app-sales-service";


    String SET_PORT_PROJECT_COMMAND = "-Dserver.port=";
    String ACADEMIC_CONTENT_SERVICE_TEST_URL = "";

}