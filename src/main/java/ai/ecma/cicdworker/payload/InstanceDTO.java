package ai.ecma.cicdworker.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class InstanceDTO {
    private String appName;
    private String ipAddr;
    private int port;
    private String instanceId;
}
