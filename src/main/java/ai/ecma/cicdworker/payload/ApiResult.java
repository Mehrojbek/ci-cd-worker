package ai.ecma.cicdworker.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ApiResult<T> {

    private T data;

    private String message;


    public static<T> ApiResult<T> successResponse(T data, String message) {
        return new ApiResult<>(data, message);
    }
}
