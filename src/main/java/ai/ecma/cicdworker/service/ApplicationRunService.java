package ai.ecma.cicdworker.service;

import ai.ecma.cicdworker.payload.ApiResult;
import ai.ecma.cicdworker.payload.InstanceDTO;
import ai.ecma.cicdworker.utils.AppConstant;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class ApplicationRunService {
    public static long milliSecund = 5000;
    public static final ObjectMapper objectMapper = new ObjectMapper();
    private static final TypeFactory typeFactory = TypeFactory.defaultInstance();
    public static String activeProfile = "ser";


    //GIT DAN SO'NNGI O'ZGARISHLARNI OLIB MAVEN CLEAN INSTALL QILADI
    //ISHLAB TURGAN SERVICE NI O'CHIRIB GITDAN SO'NGGI RUN QILADI
    @SneakyThrows
    public ApiResult<?> stopAndRunApplication(Map<String, Object> map) {

        log.info(map.toString());
        //EVENT TURI
        Object eventType = map.get("object_kind");

        //EVENT TYPE ALBATTA BO'LSIN
        if (eventType == null)
            return null;

        //FAQAT PUSH EVENTLARNI HISOBGA OLAMIZ
        if (!eventType.toString().equals("push"))
            return null;

        //KIM PUSH QILGAN
        String whoPush = map.get("ref").toString();

        //AGAR MAIN YOKI MASTER PUSH QILMAGAN BO'LSA KERAK EMAS
        if (!(whoPush.endsWith("main") || whoPush.endsWith("master")))
            return null;

        //REPOSITORY NI OLAMIZ
        Map<String, Object> repository = (Map<String, Object>) map.get("repository");

        //REPOSITORYNI HOME_PAGE URLI
        Object homepage = repository.get("homepage");

        //HOME PAGE ALBATTA BO'LSIN
        if (homepage == null)
            return null;

        //QAYSI GA SERVICE LIGINI YOZ
        log.info(homepage.toString());

        //PROJECT MUVAFFAQIYATLI RUN BO'LDIMI
        boolean projectSuccessRun = true;
        //PROJECT RUN BO'LISHI DAVOMIDAGI LOG
        StringBuilder runningLog = new StringBuilder();

        //AGAR ACADEMIC CONTENT O'ZGARTIRILGAN BO'LSA
        if (homepage.toString().endsWith(AppConstant.ACADEMIC_CONTENT_SERVICE)) {
            //ACADEMIC_CONTENT_SERVICE DAI CICDExecutor file NI EXECUTE QILADI
            projectSuccessRun = runService(
                    AppConstant.ACADEMIC_CONTENT_SERVICE,
                    AppConstant.MASTER,
                    AppConstant.ACADEMIC_CONTENT_SERVICE_PORT_FIRST,
                    AppConstant.ACADEMIC_CONTENT_SERVICE_PORT_SECOND,
                    AppConstant.ACADEMIC_CONTENT_SERVICE_JAR_NAME,
                    runningLog,
                    AppConstant.ACADEMIC_SERVICE_NAME_ON_SERVICE_REGISTRY,
                    activeProfile
            );
            //LOG GA YOZIB QO'YADI
            writeLog(AppConstant.ACADEMIC_CONTENT_SERVICE, runningLog);

            //SALES UCHUN
        } else if (homepage.toString().endsWith(AppConstant.SALES_SERVICE)) {
            projectSuccessRun = runService(
                    AppConstant.SALES_SERVICE,
                    AppConstant.MAIN,
                    AppConstant.SALES_SERVICE_FIRST_PORT,
                    AppConstant.SALES_SERVICE_SECOND_PORT,
                    AppConstant.SALES_SERVICE_JAR_NAME,
                    runningLog,
                    AppConstant.SALES_SERVICE_NAME_ON_SERVICE_REGISTRY,
                    activeProfile
            );
            writeLog(AppConstant.SALES_SERVICE, runningLog);
        }

        if (!projectSuccessRun) {
            System.out.println(runningLog);
        }

        return ApiResult.successResponse("", "");
    }

    //LOG GA YOZIB QO'YADI
    private void writeLog(String serviceName, StringBuilder runningLog) {

        log.info("\n\n================== {} ===================\n\n", serviceName);
        log.info(runningLog.toString());
        log.info("\n\n\n");

    }


    @SneakyThrows
    public boolean runService(String serviceName, String origin,
                              String firstPort, String secondPort,
                              String jarName, StringBuilder runninLog,
                              String projectNameInServiceRegistry,
                              String activeProfile) {

        //GIT DAN PULL QILADI
        gitPullMethod(serviceName, origin, runninLog);
        System.out.println("\n==================== GIT PULL SUCCESS ===========================\n");


        mavenCleanMethod(serviceName, runninLog);
        System.out.println("\n=================== CLEAN SUCCESS ==================================\n");


        mavenInstallMethod(serviceName, runninLog);
        System.out.println("\n=================== INSTALL SUCCESS ==================================\n");


        //1-PORT HOZIR SERVER DA ISHLAB TURIPTIMI ANIQLAB BERADI
        boolean firstPortIsUsed = checkPortIsUsed(firstPort, serviceName);
        System.out.println("1-portda foydanilyaptimi => " + firstPortIsUsed);

        //2-PORT BAND MI
        boolean secondPortIsUsed = checkPortIsUsed(secondPort, serviceName);
        System.out.println("2-portda foydanilyaptimi => " + firstPortIsUsed);

        //KILL QILINISHI KERAK BO'LGAN ESKI PROJECTNI PID LARI
        List<Integer> pidList = new ArrayList<>();

        //IKKALA PORT HAM ISHLATILAYOTGAN BO'LSA 1-SINI KILL QIL
        if (firstPortIsUsed && secondPortIsUsed) {

            //1-SINI KILL QIL
            findKillPid(pidList, serviceName, firstPort, runninLog);

            firstPortIsUsed = false;

            killApplication(pidList, runninLog);

            pidList.clear();

            //KILL BO'LISHI KERAK BO'LGAN PORT NI pidList GA YIG'IB BERADI
            findKillPid(pidList, serviceName, secondPort, runninLog);
        }

        //PROJECT NING PORTI AGAR 1-PORTDAN FOYDALANILAYOTGAN BO'LSA IKKINCHI PORTDA RUN QILADI
        String projectPort = firstPortIsUsed ? secondPort : firstPort;

        //PROJECT NI RUN QILADI
        runJarFile(projectPort, jarName, serviceName, activeProfile);

        //YANGI PROJECT MUVAFFAQIYATLI RUNN BO'LSA USHBU BOOLEAN TRUE BO'LADI
        boolean projectSuccessRun = checkPortIsUsed(projectPort, serviceName);

        System.out.println(pidList);

        if (projectSuccessRun)
            System.out.println("<<<<<<<<<<<<<<<<<< SUCCES RUN PROJECT >>>>>>>>>>>>>>>>>");
        else
            System.err.println(">>>>>>>>>>>>>>>>>> ERROR: FAIL RUN PROJECT <<<<<<<<<<<<<<<<<<<<<<<<<<<");

        //AGAR PROJECT MUVAFFAQIYATLI RUN BO'LSA
        if (projectSuccessRun) {

            //MAXIMUM 60 SEKUND ISHLASIN
            LocalTime finishTime = LocalTime.now().plusSeconds(60);

            //ENDDI SERVICE MUVAFFAQIYATLI JAVOB BERYAPTIMI TEKSHIRAMIZ
            checkProjectSuccessResponse(projectPort, projectNameInServiceRegistry, finishTime);

            //ESKINI KILL QILAMIZ
            killApplication(pidList, runninLog);
        }
        return projectSuccessRun;
    }

    private void checkAllPortsAndFindKillPid() {

    }

    @SneakyThrows
    private void mavenInstallMethod(String serviceName, StringBuilder runninLog) {
        //MAVEN NI -F ORQALI O'ZINI PAPKASIDAN INSTALL QILINADI
        String install = "mvn -f " + AppConstant.BASE_PATH_UBUNTU_ERP + serviceName + "/pom.xml install";
        System.out.println(install);
        Process process1 = Runtime.getRuntime().exec(install);
        BufferedReader reader = new BufferedReader(new InputStreamReader(process1.getInputStream()));
        String line = "";
        while ((line = reader.readLine()) != null) {
            runninLog
                    .append(line)
                    .append("\n");
            System.out.println(line);
        }
        Thread.sleep(100);
    }

    @SneakyThrows
    private void mavenCleanMethod(String serviceName, StringBuilder runninLog) {
        //MAVEN NI -F ORQALI O'ZINI PAPKASIDAN CLEAN QILINADI
        String clean = "mvn -f " + AppConstant.BASE_PATH_UBUNTU_ERP + serviceName + "/pom.xml clean";
        System.out.println(clean);
        Process process = Runtime.getRuntime().exec(clean);
        //QAYTGAN JAVOB NI O'QIB OLAMIZ
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = "";
        while ((line = reader.readLine()) != null) {
            runninLog
                    .append(line)
                    .append("\n");
            System.out.println(line);
        }
        Thread.sleep(100);
    }

    @SneakyThrows
    private void gitPullMethod(String serviceName, String origin, StringBuilder runninLog) {
        String pull = "git -C " + AppConstant.BASE_PATH_UBUNTU_ERP + serviceName + "/ pull origin " + origin;
        Process pullProcess = Runtime.getRuntime().exec(pull);
        BufferedReader reader = new BufferedReader(new InputStreamReader(pullProcess.getInputStream()));
        String line = "";
        while ((line = reader.readLine()) != null) {
            runninLog
                    .append(line)
                    .append("\n");
            System.out.println(line);
        }
    }

    @SneakyThrows
    private void runJarFile(String projectPort, String jarName, String serviceName, String activeProfile) {
        //java -Dserver.port=8888 -jar myApplication.jar
        String run = "java -jar " + AppConstant.SET_PORT_PROJECT_COMMAND + projectPort + " "
                + activeProfile + " "
                + AppConstant.BASE_PATH_UBUNTU_ERP + serviceName + "/target/" + jarName + ".jar &";

        System.out.println(run);

        Thread thread = new Thread(() -> {
            try {
                ProcessBuilder runBuilder = new ProcessBuilder("bash", "-c", run);
                runBuilder.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        thread.start();
        Thread.sleep(100);
        thread.stop();
        System.out.println("\n=================== RUN SUCCESS ==================================");

        //BIROZ KUTAMIZ VA RUN QILGAN PROJECT NI TEKSHIRAMIZ
        Thread.sleep(600);
    }

    @SneakyThrows
    private void findKillPid(List<Integer> pidList, String serviceName, String portForKill, StringBuilder runninLog) {
        //SERVICE NOMIDAGI PID LARNI TOPIB LISTGA YI
        String[] commands = {"bash", "-c", "ps aux | grep java | grep Dserver | grep " + serviceName + " | grep " + portForKill + " | awk '{print $2 $11}'"};
        ProcessBuilder processBuilder = new ProcessBuilder().command(commands);

        System.out.println(Arrays.toString(commands));
        Process findPidProcess = processBuilder.start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(findPidProcess.getInputStream()));
        String line = "";
        while ((line = reader.readLine()) != null) {
            runninLog
                    .append(line)
                    .append("\n");
            getPidFromString(line, pidList);
        }
    }

    @SneakyThrows
    private void killApplication(List<Integer> pidList, StringBuilder runninLog) {
        String kill = "kill ";
        if (!pidList.isEmpty()) {
            for (Integer pid : pidList) {
                String killedPid = "KILLED PID : " + pid;
                System.out.println(killedPid);
                String command = kill + pid;
                ProcessBuilder processBuilder = new ProcessBuilder("bash", "-c", command);
                Process exec = processBuilder.start();
                BufferedReader reader = new BufferedReader(new InputStreamReader(exec.getInputStream()));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    runninLog
                            .append(line)
                            .append(" \n");
                    System.out.println(line);
                }
            }
        }
    }


    //SERVICE 503 QAYTARSA
    @SneakyThrows
    private void checkProjectSuccessResponse(String port, String projectNameInServiceRegistry, LocalTime finishTime) {

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                "http://localhost:8080/instances/all",
                HttpMethod.GET, HttpEntity.EMPTY,
                String.class
        );
        String body = responseEntity.getBody();

        List<InstanceDTO> instanceDTOList = mapJsonToObjectList(body, InstanceDTO.class);
        System.out.println(instanceDTOList);
        for (InstanceDTO instanceDTO : instanceDTOList) {
            System.out.println(instanceDTO);
            if (instanceDTO.getAppName().equals(projectNameInServiceRegistry)) {
                if (String.valueOf(instanceDTO.getPort()).equals(port)) {
                    Thread.sleep(5000);
                    return;
                }
            }
        }
        Thread.sleep(2500);

        if (LocalTime.now().isAfter(finishTime)) return;//

        checkProjectSuccessResponse(port, projectNameInServiceRegistry, finishTime);
    }


    //BERILGAN PORTDA BIRORTA JARAYON BORMI SHUNI BILIB QAYTARADI
    @SneakyThrows
    private boolean checkPortIsUsed(String port, String serviceName) {

        String command = "ps aux | grep java | grep Dserver | grep " + serviceName + " | grep " + port + " | awk '{print $2  $11}'";
        System.out.println(command);
        ProcessBuilder processBuilder = new ProcessBuilder().command("bash", "-c", command);
        Process netstatProcess = processBuilder.start();

        BufferedReader reader = new BufferedReader(new InputStreamReader(netstatProcess.getInputStream()));
        String line = "";
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
            if (line.endsWith("java")) {
                int indexOf = line.indexOf("java");
                String pid = line.substring(0, indexOf);
                try {
                    Integer.parseInt(pid);
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            System.out.println(line);
        }
        return false;
    }


    //
    private static void getPidFromString(String line, List<Integer> pids) {
        try {
            if (line.endsWith("java")) {
                int indexOf = line.indexOf("java");
                String pid = line.substring(0, indexOf);
                int pidNum = Integer.parseInt(pid);
                pids.add(pidNum);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }


    //STRING JSON NI BIZ SO'RAGAN CLASS TIPIDAGI LIST ICHIDA QAYTARADI
    public static <T> List<T> mapJsonToObjectList(String json, Class<T> clazz) throws Exception {
        List<T> list;
        list = objectMapper.readValue(json, typeFactory.constructCollectionType(ArrayList.class, clazz));
        return list;
    }
}
