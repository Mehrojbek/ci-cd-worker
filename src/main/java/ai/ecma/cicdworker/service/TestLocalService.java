package ai.ecma.cicdworker.service;

import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class TestLocalService {

    @SneakyThrows
    public void testRun() {

        Runtime runtime = Runtime.getRuntime();

        String pull = "git -C /Users/mehrojbekmavlonov/IdeaProjects/hello/ pull origin main";
        Process pullProcess = runtime.exec(pull);
        BufferedReader reader = new BufferedReader(new InputStreamReader(pullProcess.getInputStream()));
        String line = "";
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
        System.out.println("\n==================== GIT PULL SUCCESS ===========================\n");


        String clean = "mvn -f /Users/mehrojbekmavlonov/IdeaProjects/hello/pom.xml clean";
        System.out.println(clean);
        Process process = runtime.exec(clean);
        //QAYTGAN JAVOB NI O'QIB OLAMIZ
        reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }

        Thread.sleep(700);
        System.out.println("\n=================== CLEAN SUCCESS ==================================\n");

        String install = "mvn -f /Users/mehrojbekmavlonov/IdeaProjects/hello/pom.xml install";
        System.out.println(install);
        Process process1 = runtime.exec(install);
        reader = new BufferedReader(new InputStreamReader(process1.getInputStream()));
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }

        Thread.sleep(700);
        System.out.println("\n=================== INSTALL SUCCESS ==================================\n");

        List<Integer> pidList = new ArrayList<>();

        String findPid = "ps aux | grep java | grep app-academic | awk '{print $2}'";
        Process process3 = runtime.exec(findPid);
        reader = new BufferedReader(new InputStreamReader(process3.getInputStream()));
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
            getPidsFromString(line, pidList);
        }

        String kill = "kill -9 ";
        if (!pidList.isEmpty()) {
            for (Integer pid : pidList) {
                String killedPid = "KILLED PID : " + pid;
                System.out.println(killedPid);
                Process exec = runtime.exec(kill + pid);
                reader = new BufferedReader(new InputStreamReader(exec.getInputStream()));
                while ((line = reader.readLine()) != null) {
                    System.out.println(line);
                }
            }
        }


        String run = "java -jar /Users/mehrojbekmavlonov/IdeaProjects/hello/target/hello.jar &";
        Thread thread = new Thread(() -> {
            BufferedReader reader2 = null;
            try {

                ProcessBuilder processBuilder = new ProcessBuilder("bash","-c",run);
                Process start = processBuilder.start();
//                Process exec = Runtime.getRuntime().exec(run);
//                reader2 = new BufferedReader(new InputStreamReader(exec.getInputStream()));
//                String line1 = "";
//
//                while ((line1 = reader2.readLine()) != null) {
//                    System.out.println(line1.isBlank());
//                    System.out.println(line1);
//                }
            } catch (IOException e) {
                e.printStackTrace();
            }
//            finally {
//                assert reader2 != null;
//                try {
//                    reader2.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
        });
        thread.start();
//        Thread.sleep(10);
        thread.stop();
//        System.out.println(thread.isInterrupted());
//        thread.interrupt();
//        thread.stop();
//        System.out.println(thread.isInterrupted());

        System.out.println(thread.isAlive());
//        while (thread.isAlive()){
//            System.out.println("Tirik ekan");
//            Thread.sleep(1000);
//        }
        System.out.println("\n=================== RUN SUCCESS ==================================");


    }

    @SneakyThrows
    public String execCmd(String cmd) {
        Process proc = Runtime.getRuntime().exec(cmd);
        java.io.InputStream is = proc.getInputStream();
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");

        String val = "";
        if (s.hasNext()) {
            val = s.next();
        } else {
            val = "";
        }
        return val;
    }

    @SneakyThrows
    public List<Integer> bufferedRead() {
        List<Integer> pidList = new ArrayList<>();
        Runtime rt = Runtime.getRuntime();
        Process process = rt.exec("lsof -ti tcp:8080");

        //QAYTGAN JAVOB NI O'QIB OLAMIZ
        final BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = "";
        while ((line = reader.readLine()) != null) {
            getPidsFromString(line, pidList);
        }
        return pidList;
    }

    private void getPidsFromString(String line, List<Integer> pids) {
        try {
            int pid = Integer.parseInt(line);
            pids.add(pid);
//            System.out.println(pid);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @SneakyThrows
    public static void main(String[] args) {
        String findPid = "ps aux | grep java";
        ProcessBuilder processBuilder = new ProcessBuilder().command("bash","-c","ps aux | grep java | grep Dserver | grep hello | grep 8091" );
        Process process = processBuilder.start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = "";
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }

    }
}
